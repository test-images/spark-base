FROM    ubuntu:xenial

ENV     LC_ALL=C.UTF-8 LANG=C.UTF-8 DEBIAN_FRONTEND=noninteractive
RUN     echo "Asia/Tehran" > /etc/timezone \
        # && dpkg-reconfigure -f noninteractive tzdata \
        && apt-get -qq update \
        && apt-get -qq upgrade \
        && apt-get -qq install wget python-pip default-jdk

WORKDIR /tmp

#       Installing Hadoop...
RUN     wget -q http://apache.mirrors.tds.net/hadoop/common/hadoop-2.8.0/hadoop-2.8.0.tar.gz \
        && tar -xzf hadoop-2.8.0.tar.gz \
        && mv hadoop-2.8.0 /usr/local/hadoop \
        && rm hadoop-2.8.0.tar.gz

#       Installing Spark...
RUN     wget -q http://apache.mirrors.tds.net/spark/spark-2.1.0/spark-2.1.0-bin-hadoop2.7.tgz \
        && tar -xf spark-2.1.0-bin-hadoop2.7.tgz \
        && mv spark-2.1.0-bin-hadoop2.7 /usr/local/spark \
        && rm spark-2.1.0-bin-hadoop2.7.tgz

ENV     HADOOP_HOME=/usr/local/hadoop SPARK_HOME=/usr/local/spark JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre/
ENV     PATH=$PATH:$HADOOP_HOME/bin:$SPARK_HOME/bin

RUN     wget http://dl.bintray.com/spark-packages/maven/datastax/spark-cassandra-connector/2.0.1-s_2.11/spark-cassandra-connector-2.0.1-s_2.11.jar
RUN     pip install -U pip

WORKDIR $SPARK_HOME
CMD     ./bin/spark-shell --jars spark-cassandra-connector-2.0.1-s_2.11.jar --conf spark.cassandra.connection.host=127.0.0.1
